include:
  - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'

default:
  image: registry.gitlab.com/centos/kmods/ci-image:latest
  before_script:
    - git config --global user.email 'sig-kmods@centosproject.org'
    - git config --global user.name 'Kmods SIG'

variables:
  GIT_STRATEGY: none
  EL: 9
  REPO: packages-main
  MOCK: almalinux-9

stages:
  - sync
  - build

mock:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "true" && $CI_PROJECT_NAMESPACE =~ /CentOS\/kmods\/.*/
      when: never
    - when: always
  parallel:
    matrix:
      - ARCH: [x86_64]
  stage: build
  script:
    - git clone --quiet --depth 1 --branch $CI_COMMIT_REF_NAME --single-branch $CI_REPOSITORY_URL .
    - 'git diff $(printf "" | git hash-object -t tree --stdin) HEAD src licenses > distro/source-git.patch'
    - mock -r $MOCK-$ARCH --define "dist .el$EL" --buildsrpm --spec distro/*.spec --source distro --resultdir .
    - rm -f distro/source-git.patch
    - mock -r $MOCK-$ARCH --define "dist .el$EL" --resultdir . *.src.rpm
  artifacts:
    name: "$CI_PROJECT_NAME-mock-$ARCH"
    untracked: true
    when: always

cbs:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "true" && $CI_PROJECT_NAMESPACE =~ /CentOS\/kmods\/.*/
  stage: build
  script:
    - cbs version
    - git clone --quiet --depth 1 --branch $CI_COMMIT_REF_NAME --single-branch $CI_REPOSITORY_URL .
    - 'git diff $(printf "" | git hash-object -t tree --stdin) HEAD src licenses > distro/source-git.patch'
    - rpmbuild --define "dist .el$EL" --define "_srcrpmdir $PWD" --define "_sourcedir $PWD/distro" -bs distro/*.spec
    - NVR=$(rpmspec -q --srpm --define "dist .el$EL" --queryformat='%{name}-%{version}-%{release}\n' distro/*.spec)
    - 'cbs buildinfo $NVR 2>/dev/null | grep >/dev/null "State: COMPLETE" || cbs --cert=$CBS build --wait --scratch kmods$EL-$REPO-el$EL *.src.rpm'
    - KERNEL=$(sed -rn "s/%global\s*kernel\s*(.*)$/\1/p" distro/*.spec)
    - |
      if git clone --quiet --branch c$EL/$KERNEL --single-branch $(echo $CI_PROJECT_URL | sed "s/https:\/\//&kmods:$GITLAB@/" | sed 's@/src/@/rpms/@g') dist-git 2>/dev/null
      then
        DNVR=$(rpmspec -q --srpm --define "dist .el$EL" --queryformat='%{name}-%{version}-%{release}\n' dist-git/*.spec)
        rpmdev-vercmp "$NVR" "$DNVR" || RETVAL=$?
        if ! [[ $RETVAL == 11 ]]
        then
          echo "Version to be deployed is not newer than version in dist-git."
          exit 0
        fi
      else
        git init -b c$EL/$KERNEL dist-git
        git -C dist-git remote add origin $(echo $CI_PROJECT_URL | sed "s/https:\/\//&kmods:$GITLAB@/" | sed 's@/src/@/rpms/@g')
      fi
    - |
      if ! [ -e distro/README.md ]
      then
        echo "This is an auto-generated dist-git repository. See $CI_PROJECT_URL for source-git repository." > distro/README.md
      fi
    - |
      if ! [ -e distro/sources ]
      then
        touch distro/sources
      fi
    - \cp -rfT distro dist-git
    - cd dist-git
    - git add .
    - git commit -m "kABI tracking kmod package (kernel >= $KERNEL)"
    - git push origin c$EL/$KERNEL

kabi:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_REF_PROTECTED == "true" && $CI_PROJECT_NAMESPACE =~ /CentOS\/kmods\/.*/ && $CI_COMMIT_REF_NAME =~ /c.*\/main/
  stage: sync
  script:
    - cbs version
    - git clone --quiet --branch $CI_COMMIT_REF_NAME --single-branch $(echo $CI_PROJECT_URL | sed "s/https:\/\//&kmods:$GITLAB@/") .
    - NAME=$(rpmspec -q --srpm --define "dist .el$EL" --queryformat='%{name}\n' distro/*.spec)
    - NVR=$(rpmspec -q --srpm --define "dist .el$EL" --queryformat='%{name}-%{version}-%{release}\n' distro/*.spec)
    - 'cbs buildinfo $NVR 2>/dev/null | grep >/dev/null "State: COMPLETE" || exit 1'
    - KERNEL=$(sed -rn "s/%global\s*kernel\s*(.*)$/\1/p" distro/*.spec)
    - CKERNEL=$KERNEL
    - git clone --quiet --branch c$EL --single-branch https://gitlab.com/CentOS/kmods/kabi
    - |
      for KVERSION in $(git -C kabi tag -l | sed -rn "s/^c$EL\/(.*)\$/\1/p" | sed 's/^/kernel-/' | rpmdev-sort | sed 's/^kernel-//' | sed -e "0,/$KERNEL/d")
      do
        for ARCH in $(grep "ExclusiveArch" distro/*.spec | awk -F ":" '{print $2}')
        do
            RPM="https://cbs.centos.org/kojifiles/packages/$(rpmspec -q --define "dist .el$EL" --queryformat="%{name}/%{version}/%{release}/$ARCH/%{name}-%{version}-%{release}.$ARCH.rpm\n" distro/*.spec)"
            curl -L -f -s -I -o /dev/null $RPM
            while read -r SVERSION SYMBOL
            do
              if ! git -C kabi show c$EL/$KVERSION:Module.symvers.$ARCH | grep "^$SVERSION\s*$SYMBOL" >/dev/null
              then
                [[ ${PIPESTATUS[0]} -eq 0 ]]
                KERNEL=$KVERSION
                break 3
              fi
            done < <(curl -L -f -s -N $RPM | rpm -qp --requires - | sed -rn 's@^kernel\((.*)\) = (0x[0-9a-f]*)$@\2\t\1@p' | sort -k2)
        done
        if [[ $(echo $KERNEL | sed -rn 's/([0-9\.]+-[0-9]+).*/\1/p') != $(echo $KVERSION | sed -rn 's/([0-9\.]+-[0-9]+).*/\1/p') ]]
        then
          KERNEL=$KVERSION
          break
        fi
      done
      if [[ "$CKERNEL" != "$KERNEL" ]]
      then
        git push -o ci.skip origin HEAD:refs/heads/c$EL/$CKERNEL
        sed -i "s/%global\s*kernel\s*.*/%global kernel $KERNEL/" distro/*.spec
        sed -i "s/%global\s*baserelease\s*.*/%global baserelease 0/" distro/*.spec
        sed -i '/^%changelog$/q' distro/*.spec
        rpmdev-bumpspec -c "kABI tracking kmod package (kernel >= $KERNEL)" -D distro/*.spec
        sed -i -e :a -e '/^\n*$/{$d;N;};/\n$/ba' distro/*.spec
        git add distro/*.spec
        git commit -m "kABI tracking kmod package (kernel >= $KERNEL)"
        git push origin HEAD:refs/heads/c$EL/main
      fi
